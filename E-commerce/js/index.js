const html = document.querySelector('html');
html.dataset.theme = `theme-light`;
function fetchTheme() {
    html.dataset.theme = localStorage.getItem('theme');
    if (html.dataset.theme == "theme-dark") {
        document.getElementById('check__box').checked = true;
    }
}
function SetTheme() {
    if (document.getElementById('check__box').checked) {
        html.dataset.theme = `theme-dark`;
        localStorage.setItem('theme', `theme-dark`);
        console.log(html.dataset.theme)
    }
    else {
        html.dataset.theme = `theme-light`;
        localStorage.setItem('theme', `theme-light`);
        console.log(html.dataset.theme)
    }
}
